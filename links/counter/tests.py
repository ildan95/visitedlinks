import unittest
import json
from django.test import Client
from unittest.mock import patch


from .views import get_domain, redis

class TestDomains(unittest.TestCase):
    def test_domains(self):
        links = {
            "https://ya.ru": "ya.ru",
            "https://ya.ru?q=123": "ya.ru",
            "funbox.ru": "funbox.ru",
            "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor": "stackoverflow.com",
            "http://test.com": "test.com",
            "http://test.com/hi": "test.com",
            "http://test.com?q=aaa": "test.com",
            "http://test.com/?q=aaa": "test.com",
            "http://test.com/?q=aaa&o=7": "test.com",
            "test.com/hi": "test.com",
            "test.com?q=aaa": "test.com",
            "test.com/?q=aaa&o=7": "test.com",
            "test.com/?q=aaa": "test.com",
            "test.com/?q=aaa?1": "test.com",
        }

        for link in links:
            self.assertEqual(get_domain(link), links[link])


class TestApi(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.domains_url = '/visited_domains'
        self.links_url = '/visited_links'
        redis.flushdb()

    def downGear(self):
        redis.flushdb()

    def get_post_response(self, url, params):
        return self.client.post(url, data=json.dumps(params), content_type='application/json')
    
    def get_get_response(self, url, params={}):
        return self.client.get(url, params)

    def get_content(self, response):
        return json.loads(response.content)

    def test_visited_links_with_bad_data(self):
        params = {
            "linkss": [
                "https://ya.ru",
                "https://ya.ru?q=123",
                "funbox.ru",
                "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
            ]
        }

        response = self.get_post_response(self.links_url, params)
        content = self.get_content(response)
        self.assertEqual(response.status_code, 200, 'Expected 200 status code')
        self.assertNotEqual(content['status'], 'ok', 'Expected error status')

    def good_visited_links(self, params):
        response = self.get_post_response(self.links_url, params)
        content = self.get_content(response)
        self.assertEqual(response.status_code, 200, 'Expected 200 status code')
        self.assertEqual(content['status'], 'ok', 'Expected ok')

    def test_not_awailable_methods(self):
        response = self.get_get_response(self.links_url)
        self.assertNotEqual(response.status_code, 200, 'Expected not 200 status code')

        response = self.client.put(self.links_url, content_type='application/json')
        self.assertNotEqual(response.status_code, 200, 'Expected not 200 status code')

        response = self.client.delete(self.links_url, content_type='application/json')
        self.assertNotEqual(response.status_code, 200, 'Expected not 200 status code')

        response = self.get_post_response(self.domains_url, {})
        self.assertNotEqual(response.status_code, 200, 'Expected not 200 status code')

        response = self.client.put(self.domains_url, content_type='application/json')
        self.assertNotEqual(response.status_code, 200, 'Expected not 200 status code')

        response = self.client.delete(self.domains_url, content_type='application/json')
        self.assertNotEqual(response.status_code, 200, 'Expected not 200 status code')


    def test_visited_links(self):
        params = {
            "links": [
                "https://ya.ru",
                "https://ya.ru?q=123",
                "funbox.ru",
                "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor",
                "test.com",
                "test2.com",
            ]
        }
        self.good_visited_links(params)

        params = {
            "links": [
                "https://ya.ru"
            ]
        }
        self.good_visited_links(params)

        params = {
            "links": [
            ]
        }
        self.good_visited_links(params)

    def test_visited_domains(self):
        self.test_visited_links()
        params = {
        }
        response = self.get_get_response(self.domains_url, params)
        self.check_visited_domains_200(response)

    def check_visited_domains_200(self, response):
        content = self.get_content(response)
        self.assertEquals(response.status_code, 200, 'Expected 200 status code')
        self.assertEquals(content['status'], "ok", 'Expected ok status')
        self.assertIsInstance(content['domains'], list, 'Expected list of domains')

    def visited_domains_count(self, params, count):
        url = self.links_url
        
        self.get_post_response(url, params)

        r = self.get_get_response(self.domains_url)
        content = self.get_content(r)
        self.check_visited_domains_200(r)
        self.assertEquals(len(content['domains']), count, 'Expected 200 status code')

    def test_visited_domains_query(self):
        params = {
            "links": [
                "https://ya.ru",
                "https://ya.ru?q=123",
            ]
        }
        self.visited_domains_count(params, 1)
    
    def test_visited_domains_http(self):
        params = {
            "links": [
                "https://ya.ru",
                "http://ya.ru",
            ]
        }
        self.visited_domains_count(params, 1)
    
    def test_visited_domains_no_protocol(self):
        params = {
            "links": [
                "ya.ru",
                "https://ya.ru",
            ]
        }
        self.visited_domains_count(params, 1)
    
    def test_visited_domains_path(self):
        params = {
            "links": [
                "https://ya.ru/test/",
                "https://ya.ru",
            ]
        }
        self.visited_domains_count(params, 1)
    
    def test_visited_domains_2_query(self):
        params = {
            "links": [
                "https://ya.ru?1?2",
                "https://ya.ru?1",
            ]
        }
        self.visited_domains_count(params, 1)
    
    def test_visited_domains_many(self):
        params = {
            "links": [
                "ya.ru",
                "https://ya.ru",
                "https://ya.ru/12?q=1",
                "ya.ru/12",
                "ws://ya.ru/12",
            ]
        }
        self.visited_domains_count(params, 1)

    def test_visited_domains_with_time(self):
        from time import time, sleep
        first_time = time()
        
        params = {
            "links": [
                "https://ya.ru",
                "https://ya2.ru",
            ]
        }

        self.get_post_response(self.links_url, params)

        r = self.get_get_response(self.domains_url, {"to": first_time})
        content = self.get_content(r)
        self.check_visited_domains_200(r)
        self.assertEquals(len(content['domains']), 2, 'Expected 200 status code')

        sleep(1)
        second_time = time()
        params = {
            "links": [
                "https://ya3.ru",
            ]
        }
        self.get_post_response(self.links_url, params)

        r = self.get_get_response(self.domains_url, {"to": second_time})
        content = self.get_content(r)
        self.check_visited_domains_200(r)
        self.assertEquals(len(content['domains']), 3, 'Expected 200 status code')

        r = self.get_get_response(self.domains_url, {"from": first_time, "to": second_time})
        content = self.get_content(r)
        self.assertEquals(len(content['domains']), 1, 'Expected 200 status code')

        sleep(1)
        third_time = time()
        params = {
            "links": [
                "https://ya4.ru",
                "https://ya5.ru",
                "https://ya6.ru",
                "https://ya7.ru",
            ]
        }
        self.get_post_response(self.links_url, params)

        r = self.get_get_response(self.domains_url, {"from": first_time})
        content = self.get_content(r)
        self.assertEquals(len(content['domains']), 5, 'Expected 200 status code')

        #same links saved too
        sleep(1)
        fourth_time = time()
        params = {
            "links": [
                "https://ya4.ru",
                "https://ya5.ru",
                "https://ya6.ru",
                "https://ya7.ru",
            ]
        }
        self.get_post_response(self.links_url, params)

        r = self.get_get_response(self.domains_url, {"from": first_time, "to": third_time})
        content = self.get_content(r)
        self.assertEquals(len(content['domains']), 5, 'Expected 200 status code')


    
