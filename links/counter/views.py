import json
from django.conf import settings
import redis
from random import random
from time import time

from urllib.parse import urlparse

from rest_framework.decorators import api_view
from django.http import JsonResponse


redis = redis.StrictRedis(host=settings.REDIS_HOST,
                                  port=settings.REDIS_PORT, db=0)


def get_domain(s):
    parsed_uri = urlparse(s)
    if parsed_uri.netloc == "":
        return parsed_uri.path.split('/')[0].split("?")[0]
    return parsed_uri.netloc

@api_view(['POST'])
def visited_links(request, *args, **kwargs):
    data = json.loads(request.body)
    if not 'links' in data:
        return JsonResponse({"status": "links list not in request data"})
    for link in data['links']:
        if redis.zscore("links", link):
            link += "?{}".format(random())
        redis.zadd("links", {link: int(time())})
    return JsonResponse({"status": "ok"})


@api_view(['GET'])
def visited_domains(request, *args, **kwargs):
    from_time = request.GET.get('from', 0)
    to_time = request.GET.get('to', int(time()))
    
    links = redis.zrangebyscore("links", from_time, to_time)

    unique_keys = set()
    for link in links:
        result = get_domain(link.decode("utf-8"))
        unique_keys.add(result)
    return JsonResponse({"status": "ok", "domains": list(unique_keys)})